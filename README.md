
1) run the naming server

> zipc_server

Note the endpoint which is printed out by the server and use it to set the 
following environment variable for all other test applications

> export IPC_INIT_REF=<zipc_server endpoint>

2) run the CPP test server

> zipc_test_server

3) run the CPP test client

> zipc_test_client

4) run the Java test server

> export CLASSPATH=<zipc package path>/externals/*:$CLASSPATH
> java atlas.daq.ipc.TestServer

5) run the Java test client

> export CLASSPATH=<your work area>/zipc/externals/*:<your work area>/$CMTCONFIG/zipc/*:$CLASSPATH
> java atlas.daq.ipc.TestClient