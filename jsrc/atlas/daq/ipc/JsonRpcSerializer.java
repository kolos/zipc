package atlas.daq.ipc;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class JsonRpcSerializer implements ISerializer {
  Gson gson = new Gson();
  
  public class RequestWriter {
    public final String jsonrpc="2.0";
    public final String method;
    public final List<Object> params;
    public final Long id;

    public RequestWriter(final Request req) {
	    this.method = req.method.getName();
	    this.params = req.params;
	    this.id = req.id;
	}
  }

  public class RequestReader {
    public String jsonrpc;
    public String method;
    public JsonArray params;
    public Long id;
  }

  public class Error {
	  public int code;
	  public String message;
	  public JsonElement data;
  }
  
  public class ResponseWriter {
	public final String jsonrpc="2.0";
    public final Object result;
    public final Error error;
    public final Long id;
    
    public ResponseWriter(final Object result, final Error error, long id) {
	    this.result = result;
	    this.error = error;
	    this.id = id;
	}
  }
  
  public class ResponseReader {
	public String jsonrpc="2.0";
    public JsonElement result;
    public Error error;
    public Long id;
  }
  
  @Override
  public String serializeRequest(final Request req) {
    return gson.toJson(new RequestWriter(req));
  }

  @Override
  public Request deserializeRequest(final String json, final Map<String, Method> methods) {
	  RequestReader request = gson.fromJson(json, RequestReader.class);
      List<Object> params = new ArrayList<>();
      
      Method method = methods.get(request.method);
      Class<?>[] ptypes = method.getParameterTypes();
      
      if (request.params.size() != ptypes.length) {
          System.err.println("Wrong number of parameters (" + request.params.size() + ") provided for the '" + method + "' method");
    	  return null;
      }
      for (int i=0; i < ptypes.length; ++i) {
    	  params.add(gson.fromJson(request.params.get(i), ptypes[i]));
      }
      
      return new ISerializer.Request(method, params, request.id);
  }

  @Override
  public String serializeResponse(final Object resp, final Exception e, long id) {
    return gson.toJson(new ResponseWriter(resp, null, id));
  }

  @Override
  public <T> T deserializeResponse(final String str, final Class<T> clazz) throws Exception {
	  ResponseReader resp = gson.fromJson(str, ResponseReader.class);
	  if (resp.error != null) {
		  // FIXME: properly read ERS exception
		  Exception e = gson.fromJson(resp.error.data, Exception.class);
		  throw e;
	  } else {
		  return gson.fromJson(resp.result, clazz);
	  }
  }
}
