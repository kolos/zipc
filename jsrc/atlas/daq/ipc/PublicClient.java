package atlas.daq.ipc;

public class PublicClient extends Client {
  private final String name;
  private final Partition partition;

  public PublicClient(final SocketType type, final Partition partition, final String name, final Context context) {
    super(type, partition.lookup(name), context);
    this.name = name;
    this.partition = partition;
  }

  public PublicClient(final SocketType type, final Partition partition, final String name) {
    this(type, partition, name, Context.globalInstance());
  }

  public String getName() {
    return name;
  }

  public Partition getPartition() {
    return partition;
  }

  boolean recover() {
    try {
      Endpoint e = partition.lookup(name);
      if (e != endpoint) {
        socket = new Socket(socket.getSocketType(), socket.getContext());
        connect(e);
        System.out.println("recovery succeeded with the new endpoint: " + e.getAddress());
        return true;
      } else {
        System.out.println("recovery failed");
      }
    } catch (Exception e) {
      System.out.println("recovery failed: " + e.getMessage());
    }
    return false;
  }
}
