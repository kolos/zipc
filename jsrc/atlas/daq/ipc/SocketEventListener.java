package atlas.daq.ipc;

import org.zeromq.ZMQ;

public interface SocketEventListener {
  default void accepted(ZMQ.Event event) {};

  default void acceptFailed(ZMQ.Event event) {};

  default void bindFailed(ZMQ.Event event) {};

  default void closed(ZMQ.Event event) {};

  default void closeFailed(ZMQ.Event event) {};

  default void connected(ZMQ.Event event) {};

  default void connectDelayed(ZMQ.Event event) {};

  default void connectRetried(ZMQ.Event event) {};

  default void disconnected(ZMQ.Event event) {};

  default void listening(ZMQ.Event event) {};

  default void unknown(ZMQ.Event event) {};
}
