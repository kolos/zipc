package atlas.daq.ipc;

import org.zeromq.ZMQ;

public class Monitor extends org.zeromq.ZMQ.Socket {
  private static final String MONITOR_PREFIX = "inproc://monitor.";

  private final Socket socket;

  public Monitor(final Socket socket, int events) {
    super(socket.getContext(), SocketType.pair.getValue());
    this.socket = socket;
    String address = MONITOR_PREFIX + socket.hashCode();
    socket.monitor(address, events);
    connect(address);
  }

  public Monitor(final Socket socket) {
    super(socket.getContext(), SocketType.pair.getValue());
    this.socket = socket;
    String address = MONITOR_PREFIX + socket.hashCode();
    socket.monitor(address, ZMQ.EVENT_CONNECTED | ZMQ.EVENT_CONNECT_RETRIED | ZMQ.EVENT_DISCONNECTED);
    connect(address);
  }

  public void notify(final SocketEventListener listener) {
    while (true) {
      ZMQ.Event event = ZMQ.Event.recv(this, ZMQ.NOBLOCK);
      if (event == null) {
        break;
      }

      switch (event.getEvent()) {
      case ZMQ.EVENT_CONNECTED:
        socket.connected(event);
        break;
      case ZMQ.EVENT_CONNECT_DELAYED:
        socket.connectDelayed(event);
        break;
      case ZMQ.EVENT_CONNECT_RETRIED:
        socket.connectRetried(event);
        break;
      case ZMQ.EVENT_LISTENING:
        socket.listening(event);
        break;
      case ZMQ.EVENT_BIND_FAILED:
        socket.bindFailed(event);
        break;
      case ZMQ.EVENT_ACCEPTED:
        socket.accepted(event);
        break;
      case ZMQ.EVENT_ACCEPT_FAILED:
        socket.acceptFailed(event);
        break;
      case ZMQ.EVENT_CLOSED:
        socket.closed(event);
        break;
      case ZMQ.EVENT_CLOSE_FAILED:
        socket.closeFailed(event);
        break;
      case ZMQ.EVENT_DISCONNECTED:
        socket.disconnected(event);
        break;
      default:
        socket.unknown(event);
        break;
      }
    }
  }
}
