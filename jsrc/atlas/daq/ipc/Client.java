package atlas.daq.ipc;

import java.util.Date;

public class Client {
  private static final int initTimeout() {
    try {
      return Integer.valueOf(java.lang.System.getenv("TDAQ_IPC_TIMEOUT"));
    } catch (Exception e) {
      return 10000;
    }
  };

  private static final int DEFAULT_TIMEOUT = initTimeout();
  private ISerializer serializer = ISerializer.DEFAULT_SERIALIZER;

  protected Endpoint endpoint;
  protected Socket socket;

  public Client(final SocketType type, final Endpoint endpoint, final Context context) {
    socket = new Socket(type, context);
    connect(endpoint);
  }

  public Client(final SocketType type, final Context context) {
    socket = new Socket(type, context);
  }

  public Client(final SocketType type, final Endpoint endpoint) {
    this(type, endpoint, Context.globalInstance());
  }

  public Endpoint getEndpoint() {
    return endpoint;
  }

  public Socket getSocket() {
    return socket;
  }

  public String recv(int milliseconds) {
    while (true) {
      if (socket.isFailed()) {
        throw new RuntimeException("Socket is not connected");
      }

      if (socket.canRead(milliseconds)) {
        return socket.recvStr();
      }
    }
  }

  public String recv() {
    return recv(DEFAULT_TIMEOUT);
  }

  public void send(final String message, int milliseconds) {
    while (true) {
      if (socket.isFailed() && !recover()) {
        throw new RuntimeException("Socket is not connected");
      }

      if (socket.canWrite(milliseconds)) {
        socket.send(message);
        break;
      }
    }
  }

  public void send(final String message) {
    send(message, DEFAULT_TIMEOUT);
  }

  public String makeRequest(final String message) {
    return makeRequest(message, DEFAULT_TIMEOUT);
  }

  public String makeRequest(final String message, int milliseconds) {
    Date start = new Date();
    send(message, milliseconds);
    Date end = new Date();

    int passed = (int) (end.getTime() - start.getTime());
    if (passed > milliseconds) {
      throw new RuntimeException("TIMEOUT");
    }

    return recv(milliseconds - passed);
  }

  public void connect(final Endpoint ep) {
    socket.connect(ep.getAddress());
    this.endpoint = ep;
  }

  public void disconnect() {
    socket.connect(endpoint.getAddress());
  }

  boolean recover() {
    return false;
  }

  public ISerializer getSerializer() {
    return serializer;
  }

  public void setSerializer(final ISerializer serializer) {
    this.serializer = serializer;
  }

}
