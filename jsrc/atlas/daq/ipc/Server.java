package atlas.daq.ipc;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import zmq.ZMQ;

public class Server {
  protected Socket socket;
  protected Endpoint endpoint;
  protected WorkerThreads workers;
  private ISerializer serializer;
  private Context context;

  public static class Builder {
    // required
    private final SocketType socketType;
    private final Consumer<Socket> requestHandler;

    // optional
    private Endpoint endpoint = new Endpoint();
    private int workerThreadsNumber = 1;
    private int ioThreadsNumber = 1;
    private ISerializer serializer = ISerializer.DEFAULT_SERIALIZER;

    public Builder(final SocketType socketType, final RequestHandler requestHandler) {
      this.socketType = socketType;
      this.requestHandler = socket -> {
        while (true) {
          try {
            String request = socket.recvStr();
            String reply = null;
            try {
              reply = requestHandler.process(request);
            } catch (Exception e) {
              // TODO serialize exception and pass it to the requester
              continue;
            }
            if (reply != null) {
              socket.send(reply);
            }
          } catch (Exception e) {
            break;
          }
        }
      };
    }

    public Builder(final SocketType socketType, final Object requestHandler) {
      this.socketType = socketType;
      this.requestHandler = socket -> {
        Class<?> clazz = requestHandler.getClass();
      	Map<String, Method> methods = Stream.of(clazz.getDeclaredMethods())
      			.collect(Collectors.toMap(Method::getName, Function.identity()));

        while (true) {
          String req = socket.recvStr();
          try {
            ISerializer.Request request = serializer.deserializeRequest(req, methods);
            
            Object resp = null;
            try {
            	resp = request.method.invoke(requestHandler, request.params.toArray(new Object[request.params.size()]));
            } catch (Exception e) {
	          if (request.method.getAnnotation(Oneway.class) == null) {
	            socket.send(serializer.serializeResponse(null, e, request.id));
              } else {
                  System.err.println("The method '" + request.method.getName()
                      + "' is declared as oneway but it throws exception: " + e.getMessage());
              }
              continue;
            }
            
            if (request.method.getAnnotation(Oneway.class) == null) {
              socket.send(serializer.serializeResponse(resp, null, request.id));
            } else {
              if (request.method.getReturnType() != Void.TYPE) {
                System.err.println("The method '" + request.method.getName()
                        + "' is declared as oneway but has non-void return type. Return value will be lost.");
              }
            }
          } catch (Exception e) {
            System.err.println("Exception while processing external request: " + e.getMessage());
            e.printStackTrace();
          }
        }
      };
    }

    public Builder endpoint(final Endpoint endpoint) {
      this.endpoint = endpoint;
      return this;
    }

    public Builder workerThreads(int threadsNumber) {
      this.workerThreadsNumber = threadsNumber;
      return this;
    }

    public Builder ioThreads(int threadsNumber) {
      this.ioThreadsNumber = threadsNumber;
      return this;
    }

    public Builder serializer(final ISerializer serializer) {
      this.serializer = serializer;
      return this;
    }

    public Server build() {
      return new Server(this);
    }
  }

  private Server(final Builder builder) {
    this.context = new Context(builder.ioThreadsNumber);
    this.socket = new Socket(SocketType.router, this.context);
    this.workers = new WorkerThreads(socket, builder.socketType, Collections.nCopies(
        builder.workerThreadsNumber, builder.requestHandler));
    this.endpoint = builder.endpoint;
    this.serializer = builder.serializer;
    bind();
  }

  public Endpoint getEndpoint() {
    return endpoint;
  }

  public ISerializer getSerializer() {
    return serializer;
  }

  void bind() {
    try {
      socket.bind(endpoint.getAddress());
    } catch (Exception e) {
      System.err.println("Can't bind to the given endpoint: " + e.getMessage());
      throw e;
    }

    String address = (String) socket.base().getsockoptx(ZMQ.ZMQ_LAST_ENDPOINT);
    endpoint = Endpoint.fromString(address);
  }

  void unbind() {
    try {
      socket.unbind(endpoint.getAddress());
    } catch (Exception e) {
      System.err.println("Can't unbind from the given endpoint: " + e.getMessage());
    }
  }

}
