package atlas.daq.ipc;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.zeromq.ZMQ;

public class WorkerThreads {
  private final String name;
  private final Socket dealer;
  private final Proxy proxy;
  private final List<Worker> workers;

  final class Proxy extends Thread {
    private final Socket frontend;
    
    Proxy(Socket frontend) {
      this.frontend = frontend;
    }
    
    public void run() {
      ZMQ.proxy(frontend, dealer, null);
    }
  }
  
  final class Worker extends Thread {
    private final Socket backend;
    private final Consumer<Socket> handler;
   
    Worker(final Socket backend, final Consumer<Socket> handler) {
      this.backend = backend;
      this.handler = handler;
    }
    
    public void run() {
      handler.accept(backend);
    }
  }
  
  public WorkerThreads(final Socket socket, final SocketType type, final List<Consumer<Socket>> handlers) {
    name = "inproc://workers_" + this.hashCode();
    dealer = new Socket(SocketType.dealer, socket.getContext());
    dealer.bind(name);
    proxy = new Proxy(socket);
    workers = handlers.stream().map(h -> {
        Socket s = new Socket(type, socket.getContext());
        s.connect(name);
        return new Worker(s, h);
      }).collect(Collectors.toList());
    proxy.start();
    workers.forEach(w -> w.start());
  }

  Socket getWorkersSocket() {
    return dealer;
  }

  String getName() {
    return name;
  }
}
