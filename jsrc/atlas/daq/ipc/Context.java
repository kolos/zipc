package atlas.daq.ipc;

import org.zeromq.ZMQ;

public class Context extends ZMQ.Context {
  private static final Context instance = new Context();

  public Context() {
    this(1);
  }

  public Context(int ioThreads) {
    super(ioThreads);
  }

  public static final Context globalInstance() {
    return instance;
  }
}
