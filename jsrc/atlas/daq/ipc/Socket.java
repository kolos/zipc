package atlas.daq.ipc;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.PollItem;
import org.zeromq.ZMQ.Poller;

class Socket extends ZMQ.Socket implements SocketEventListener {
  private final Monitor monitor;
  private final Context context;
  private final SocketType type;
  private boolean failed;

  public Socket(final SocketType type, final Context context) {
    super(context, type.getValue());
    this.type = type;
    this.context = context;
    this.failed = false;
    this.monitor = new Monitor(this);
  }

  public boolean canRead(int timeout) {
    return poll(Poller.POLLIN, timeout) && !failed;
  }

  public boolean canWrite(int timeout) {
    return poll(Poller.POLLOUT, timeout) && !failed;
  }

  public Context getContext() {
    return context;
  }

  public SocketType getSocketType() {
    return type;
  }

  public boolean isFailed() {
    return failed;
  }

  private boolean poll(int event, int timeout) {
    PollItem items[] = { new PollItem(this, event | Poller.POLLERR),
        new PollItem(monitor, Poller.POLLIN | Poller.POLLERR) };
    int rc = ZMQ.poll(items, timeout);
    if (rc == -1) {
      failed = true;
      System.out.println("poll: rc = -1");
      return false; // TODO: throw here ?
    }

    if (rc == 0) {
      System.out.println("poll: rc = 0");
      throw new RuntimeException("TIMEOUT");
    }

    if (items[1].isReadable()) {
      System.out.println("poll: monitor socket has events");
      monitor.notify(this);
    }

    if (items[0].isError()) {
      failed = true;
      System.out.println("poll: socket is in error state");
      return false; // TODO: may be throw here ?
    }

    if (event == Poller.POLLIN && items[0].isReadable()) {
      return true;
    } else if (event == Poller.POLLOUT && items[0].isWritable()) {
      return true;
    }

    // must not happen
    return false;
  }

  @Override
  public void connected(final ZMQ.Event event) {
    System.out.println(event.getAddress() + " socket is connected");
    failed = false;
  }

  @Override
  public void connectRetried(final ZMQ.Event event) {
    System.out.println(event.getAddress() + " retrying connection attempt");
    failed = true;
  }

  @Override
  public void disconnected(final ZMQ.Event event) {
    System.out.println(event.getAddress() + " socket is disconnected");
    failed = true;
  }

};
