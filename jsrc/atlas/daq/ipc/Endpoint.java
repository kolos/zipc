package atlas.daq.ipc;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Endpoint {
  private final Transport transport;
  private final String interface_;
  private final int port;
  private final String address;

  private static final String loopbackAddress = "127.0.0.1";

  private static final String defaultHost = "*";
  private static final int defaultPort = 0;
  private static final Transport defaultTransport = Transport.tcp;
  private static final Pattern pattern = Pattern
      .compile("(tcp|inproc|ipc|pgm):\\/\\/([^:]+)(?::([0-9]+))?");

  public static Endpoint fromString(String s) {
    try {
      Matcher matcher = pattern.matcher(s);
      matcher.find();

      if (matcher.groupCount() == 3) {
        return new Endpoint(Transport.valueOf(matcher.group(1)), matcher.group(2),
            Integer.valueOf(matcher.group(3)));
      } else if (matcher.groupCount() == 2) {
        return new Endpoint(Transport.valueOf(matcher.group(1)), matcher.group(2), defaultPort);
      }
    } catch (Exception e) {
      throw new RuntimeException("Bad endpoint: " + e.getMessage());
    }
    throw new RuntimeException("Bad endpoint: " + s);
  }

  public Endpoint() {
    this(defaultTransport, defaultHost, defaultPort);
  }

  public Endpoint(final String interface_) {
    this(defaultTransport, interface_, defaultPort);
  }

  public Endpoint(int port) {
    this(defaultTransport, defaultHost, port);
  }

  public Endpoint(final Transport transport, final String interface_, int port) {
    this.transport = transport;
    this.interface_ = getFirstSuitableAddress(transport, interface_);
    this.port = port;
    this.address = this.transport + "://" + this.interface_
        + (this.transport == Transport.tcp ? ":" + (this.port == 0 ? "*" : this.port) : "");
  }

  public Transport getTransport() {
    return transport;
  }

  public String getInterface() {
    return interface_;
  }

  public int getPort() {
    return port;
  }

  public String getAddress() {
    return address;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((address == null) ? 0 : address.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Endpoint other = (Endpoint) obj;
    if (address == null) {
      if (other.address != null) {
        return false;
      }
    } else if (!address.equals(other.address)) {
      return false;
    }
    return true;
  }

  private static List<String> getAllAddresses() {
    try {
      InetAddress localhost = InetAddress.getLocalHost();
      InetAddress[] allMyIps = InetAddress.getAllByName(localhost.getCanonicalHostName());
      return Arrays.stream(allMyIps).map(a -> a.getHostAddress()).collect(Collectors.toList());
    } catch (UnknownHostException e) {
      return Collections.<String> emptyList();
    }
  }

  private static List<String> getAllInterfaces() {
    try {
      return Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
          .map(i -> i.getName()).collect(Collectors.toList());
    } catch (SocketException e) {
      return Collections.<String> emptyList();
    }
  }

  private static String getFirstSuitableAddress(Transport transport, String pattern) {
    if (transport != Transport.tcp) {
      return pattern;
    }
    if (pattern.isEmpty() || pattern == "*") {
      return getAllAddresses().stream().filter(a -> !a.equals(loopbackAddress)).findFirst()
          .orElse(pattern);
    }
    return pattern;
  }
}
