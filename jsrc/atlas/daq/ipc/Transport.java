package atlas.daq.ipc;

public enum Transport {
  inproc, ipc, tcp, pgm;
}
