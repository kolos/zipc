package atlas.daq.ipc;

public class Partition {
  private static final Client namingService = init();
  private final String name;

  private static Client init() {
    try {
      return new PersistentClient(SocketType.req, Endpoint.fromString(java.lang.System
          .getenv("IPC_INIT_REF")));
    } catch (Exception e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
      return null;
    }
  }

  public Partition() {
    name = new String();
  }

  public Partition(final String name) {
    this.name = name;
  }

  public void publish(final Server server, final String name) {
    String message = "publish " + name + " " + server.getEndpoint().getAddress();

    String reply = namingService.makeRequest(message);

    if (reply.equals("no")) {
      throw new RuntimeException("Object can't be published");
    }
  }

  public void withdraw(final String name) {
    String message = "withdraw " + name;

    String reply = namingService.makeRequest(message);

    if (reply.equals("no")) {
      throw new RuntimeException("Object is not published");
    }
  }

  public Endpoint lookup(final String name) {
    String message = "resolve " + name;

    String reply = namingService.makeRequest(message);

    if (!reply.equals("no")) {
      return Endpoint.fromString(reply);
    }
    throw new RuntimeException("Object is not published");
  }

  public <T> T lookup(final Class<T> clazz, final String name, final SocketType type) {
    PublicClient client = new PublicClient(type, this, name, Context.globalInstance());
    return ISerializer.build(clazz, client, client.getSerializer());
  }

  public String getName() {
    return name;
  }

}
