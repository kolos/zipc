package atlas.daq.ipc;

public interface RequestHandler {
  String process(String msg);
}
