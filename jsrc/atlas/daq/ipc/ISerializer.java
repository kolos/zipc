package atlas.daq.ipc;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public interface ISerializer {
  static final ISerializer DEFAULT_SERIALIZER = new JsonRpcSerializer();

  public class Request {
	private static final AtomicLong gid = new AtomicLong(0);
	
	public final Method method;
    public final List<Object> params;
    public final long id;

    public Request(final Method method, final List<Object> params) {
      this.method = method;
      this.params = params;
      this.id = gid.incrementAndGet();
    }
    
    public Request(final Method method, final List<Object> params, long id) {
	    this.method = method;
	    this.params = params;
	    this.id = id;
	  }
  }

  String serializeRequest(Request req);

  Request deserializeRequest(String str, Map<String, Method> methods);

  String serializeResponse(Object resp, Exception e, long id);

  <T> T deserializeResponse(String str, Class<T> clazz) throws Exception;

  @SuppressWarnings("unchecked")
  static <T> T build(final Class<T> clazz, final Client client, final ISerializer serializer) {
    return (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] { clazz },
	    new InvocationHandler() {
	      @Override
	      public Object invoke(Object proxy, Method method, Object[] args) throws Exception {
	        Request request = new Request(method, args == null ? Collections.emptyList() : Arrays.asList(args));
	    	if (method.getAnnotation(Oneway.class) == null) {
	          String resp = client.makeRequest(serializer.serializeRequest(request));
	          if (method.getReturnType() == Void.TYPE) {
	            return null;
	          }
	          return serializer.deserializeResponse(resp, method.getReturnType());
	        } else {
	          client.send(serializer.serializeRequest(request));
	          if (method.getReturnType() != Void.TYPE) {
	            throw new RuntimeException("The method '" + method.getName()
	                + "' is declared as oneway but has non-void return type");
	          }
	          return null;
	        }
	      }
	    });
  }
}
