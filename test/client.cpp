/*
 * client.cpp
 *
 *  Created on: Apr 11, 2016
 *      Author: kolos
 */

#include <unistd.h>
#include <iostream>

#include <ipc/PublicClient.hpp>

using namespace tdaq::ipc;

void test(Client & client)
{
    //  Do N requests, waiting each time for a response
    for (int request_nbr = 0; request_nbr != 100; ++request_nbr) {
        zmq::message_t request(1);
        memset(request.data(), 'x', 1);

        std::clog << "sending request " << request_nbr << " ... ";
        client.send(request);

        std::clog << " waiting for replay ... ";
        //  Get the reply.
        zmq::message_t reply;
        client.recv(reply);
        std::clog << "got it " << std::endl;

        sleep(3);
    }
}

int main(int , char** )
{
    Partition p;
    PublicClient client(zmq::socket_type::req, p, "test");

    std::clog << "Connected to " << client.getEndpoint().getAddress()
            << std::endl;

    while (true) {
        try {
            test(client);
        }
        catch (zmq::error_t & e) {
            std::clog << "got exception: " << e.what() << std::endl;
            sleep(1);
        }
    }
    return 0;
}

