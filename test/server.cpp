/*
 * server.cpp
 *
 *  Created on: Apr 11, 2016
 *      Author: kolos
 */

#include <unistd.h>
#include <iostream>
#include <ipc/Partition.hpp>
#include <ipc/Server.hpp>

using namespace tdaq::ipc;

zmq::message_t requestHandler(zmq::message_t& request)
{
    std::clog << "received request:";
    const char* data = (const char*)request.data();
    std::clog << data << std::endl;

    //  Do some 'work'
    ::sleep(1);

    zmq::message_t reply(request.size());
    memcpy((void*)reply.data(), data, request.size());

    std::clog << "sending reply ... ";
    return reply;
}

int main(int , char** )
{
    {
        Server server(zmq::socket_type::rep, requestHandler);
        Partition p;
        p.publish(server, "test");
        std::cout << "The first incarnation is over" << std::endl;
    }

    Server server(zmq::socket_type::rep, requestHandler);

    Partition p;
    p.publish(server, "test");

    while (true) {
        sleep(1);
    }
    return 0;
}

