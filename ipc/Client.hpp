/*
 * Client.hpp
 *
 *  Created on: Apr 8, 2016
 *      Author: kolos
 */

#ifndef _IPC_CLIENT_HPP_
#define _IPC_CLIENT_HPP_

#include <chrono>

#include <zmq.hpp>
#include <ipc/Context.hpp>
#include <ipc/Endpoint.hpp>
#include <ipc/Socket.hpp>

namespace tdaq
{
    namespace ipc
    {
        class Client
        {
        protected:
            Endpoint m_endpoint;
            Socket* m_socket;

        public:
            Client(::zmq::socket_type type, Endpoint const& endpoint,
                    ::zmq::context_t& context = Context::globalInstance());

            virtual ~Client();

            void recv(zmq::message_t& m, int32_t milliseconds=-1);

            void send(zmq::message_t& m, int32_t milliseconds=-1);

            zmq::message_t makeRequest(zmq::message_t& m);

            zmq::message_t makeRequest(zmq::message_t& m, std::chrono::milliseconds timeout);

            Endpoint const& getEndpoint() const
            {
                return m_endpoint;
            }

            bool isConnected() const
            {
                return m_socket->connected();
            }

            const Socket& getSocket() const
            {
                return *m_socket;
            }

            Socket& getSocket()
            {
                return *m_socket;
            }

        protected:
            Client(::zmq::socket_type type, ::zmq::context_t& context);

            void connect(Endpoint const& endpont);

            void disconnect();

            virtual bool recover();

        private:
            Client(const Client&) = delete;
            Client& operator=(const Client&) = delete;
        };
    }
}

#endif /* _IPC_CLIENT_HPP_ */
