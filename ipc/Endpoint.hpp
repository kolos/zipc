/*
 * Endpoint.hpp
 *
 *  Created on: Apr 8, 2016
 *      Author: kolos
 */

#ifndef _IPC_ENDPOINT_HPP_
#define _IPC_ENDPOINT_HPP_

#include <ipc/Partition.hpp>

#include <zmq.hpp>

namespace tdaq
{
    namespace ipc
    {
        enum Transport
        {
            inproc, ipc, tcp, pgm
        };

        class Endpoint
        {
            Transport m_transport;
            std::string m_interface;
            uint32_t m_port;
            std::string m_address;

            static const std::string defaultInterface;
            static const uint32_t defaultPort = 0;
            static const Transport defaultTransport;

        public:
            static Endpoint fromString(std::string const & address);

            Endpoint(Transport const& transport = defaultTransport, std::string const& interface = defaultInterface,
                    uint32_t port = defaultPort);

            Endpoint(std::string const& interface, uint32_t port = defaultPort);

            Endpoint(uint32_t port);

            bool operator==(Endpoint const& e) const
            {
                return e.m_address == m_address;
            }

            bool operator!=(Endpoint const& e) const
            {
                return e.m_address != m_address;
            }

            std::string const& getAddress() const
            {
                return m_address;
            }

            std::string const& getInterface() const
            {
                return m_interface;
            }

            int getPort() const
            {
                return m_port;
            }

            Transport const& getTransport() const
            {
                return m_transport;
            }
        };
    }
}

#endif /* _IPC_ENDPOINT_HPP_ */
