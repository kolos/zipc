/*
 * Server.hpp
 *
 *  Created on: Apr 8, 2016
 *      Author: kolos
 */

#ifndef _IPC_SERVER_HPP_
#define _IPC_SERVER_HPP_

#include <functional>
#include <memory>
#include <thread>

#include <zmq.hpp>
#include <zmq_addon.hpp>

#include <ipc/Endpoint.hpp>
#include <ipc/Socket.hpp>

namespace tdaq
{
    namespace ipc
    {
        typedef std::function<zmq::message_t(zmq::message_t&)> RequestHandler;
        typedef std::function<zmq::multipart_t(zmq::multipart_t&)> MultipartRequestHandler;

        class Server
        {
        public:
            Server(::zmq::socket_type type, RequestHandler const& handler,
                    int threads = 1,
                    Endpoint const& endpoint = Endpoint());

            Server(::zmq::socket_type type, MultipartRequestHandler const& handler,
                    int threads = 1,
                    Endpoint const& endpoint = Endpoint());

            ~Server();

            Endpoint const& getEndpoint() const
            {
                return m_endpoint;
            }

            Socket const& getSocket() const
            {
                return *m_router;
            }

            Socket& getSocket()
            {
                return *m_router;
            }

        private:
            typedef std::shared_ptr<std::thread> Thread;

            void bind(Endpoint const& endpoint);

            void unbind();

            void proxy();

            void worker(::zmq::socket_type type, RequestHandler const& handler);

            void multipartWorker(::zmq::socket_type type, MultipartRequestHandler const& handler);

            std::unique_ptr<::zmq::context_t> m_context;
            std::unique_ptr<Socket> m_router;
            const std::string m_proxy_address;
            std::vector<Thread> m_workers;
            Thread m_proxy;
            Endpoint m_endpoint;
        };
    }
}

#endif /* _IPC_SERVER_HPP_ */
