/*
 * Partition.hpp
 *
 *  Created on: Apr 8, 2016
 *      Author: kolos
 */

#ifndef _IPC_PARTITION_HPP_
#define _IPC_PARTITION_HPP_

namespace tdaq
{
    namespace ipc
    {
        class Endpoint;
        class Server;

        class Partition
        {
            std::string m_name;

        public:

            Partition() = default;

            Partition(std::string const& name) :
                    m_name(name)
            {
            }

            void publish(Server const& server, std::string const& name) const;

            void withdraw(std::string const& name) const;

            Endpoint lookup(std::string const& name) const;

            const std::string& getName() const
            {
                return m_name;
            }
        };
    }
}

#endif /* _IPC_PARTITION_HPP_ */
