/*
 * PersistentClient.hpp
 *
 *  Created on: Apr 8, 2016
 *      Author: kolos
 */

#ifndef _IPC_PERSISTENT_CLIENT_HPP_
#define _IPC_PERSISTENT_CLIENT_HPP_

#include <zmq.hpp>
#include <ipc/Client.hpp>
#include <ipc/Context.hpp>
#include <ipc/Partition.hpp>

namespace tdaq
{
    namespace ipc
    {
        class PersistentClient: public Client
        {
        public:
            PersistentClient(::zmq::socket_type type, Endpoint const& endpoint,
                    ::zmq::context_t& context = Context::globalInstance());

            ~PersistentClient();

        private:
            bool recover();
        };
    }
}

#endif /* _IPC_PUBLIC_CLIENT_HPP_ */
