/*
 * PublicClient.hpp
 *
 *  Created on: Apr 8, 2016
 *      Author: kolos
 */

#ifndef _IPC_PUBLIC_CLIENT_HPP_
#define _IPC_PUBLIC_CLIENT_HPP_

#include <zmq.hpp>
#include <ipc/Client.hpp>
#include <ipc/Context.hpp>
#include <ipc/Partition.hpp>

namespace tdaq
{
    namespace ipc
    {
        class PublicClient: public Client
        {
        protected:
            Partition m_partition;
            std::string m_name;

        public:
            PublicClient(::zmq::socket_type type, Partition const& partition,
                    std::string const& name, ::zmq::context_t& context =
                            Context::globalInstance());

            ~PublicClient();

            const std::string& getName() const
            {
                return m_name;
            }

            const Partition& getPartition() const
            {
                return m_partition;
            }

        protected:
            PublicClient(::zmq::socket_type type, ::zmq::context_t& context);

            void connect(Partition const& partition, std::string const& name);

        private:
            bool recover();
        };
    }
}

#endif /* _IPC_PUBLIC_CLIENT_HPP_ */
