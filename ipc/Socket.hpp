/*
 * Socket.hpp
 *
 *  Created on: Apr 8, 2016
 *      Author: kolos
 */

#ifndef _IPC_SOCKET_HPP_
#define _IPC_SOCKET_HPP_

#include <zmq.hpp>

#include <ipc/Context.hpp>
#include <ipc/Monitor.hpp>

namespace tdaq
{
    namespace ipc
    {
        class Socket: public ::zmq::socket_t, public Context, public Monitor
        {
        private:
            ::zmq::socket_type m_type;
            bool m_failed;

            bool poll(int event, int32_t timeout);

        protected:
            void on_event_disconnected(const zmq_event_t& event,
                    const char* addr);

            void on_event_connected(const zmq_event_t& event, const char* addr);

            void on_event_connect_retried(const zmq_event_t& event, const char* addr);

        public:

            Socket(::zmq::socket_type type, ::zmq::context_t& context);

            bool canRead(int32_t timeout);

            bool canWrite(int32_t timeout);

            bool isFailed() const
            {
                return m_failed;
            }

            ::zmq::socket_type getType() const
            {
                return m_type;
            }
        };
    }
}

#endif /* _IPC_SOCKET_HPP_ */
