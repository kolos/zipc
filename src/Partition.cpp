/*
 * Partition.cpp
 *
 *  Created on: Apr 11, 2016
 *      Author: kolos
 */

#include <iostream>

#include <ipc/Endpoint.hpp>
#include <ipc/Partition.hpp>
#include <ipc/Server.hpp>
#include <ipc/PersistentClient.hpp>

using namespace tdaq::ipc;

namespace
{
    tdaq::ipc::Client& client()
    {
        static tdaq::ipc::PersistentClient client(zmq::socket_type::req,
                Endpoint::fromString(std::string(::getenv("IPC_INIT_REF") ? ::getenv("IPC_INIT_REF") : "")));
        return client;
    }
}

void tdaq::ipc::Partition::publish(const Server& server,
        const std::string& name) const
{
    std::string s("publish " + name + " " + server.getEndpoint().getAddress());

    zmq::message_t request(s.size());
    memcpy(request.data(), s.c_str(), s.size());

    zmq::message_t reply = client().makeRequest(request);
}

void tdaq::ipc::Partition::withdraw(const std::string& name) const
{
    std::string s("withdraw " + name);

    zmq::message_t request(s.size());
    memcpy(request.data(), s.c_str(), s.size());

    zmq::message_t reply = client().makeRequest(request);
}

tdaq::ipc::Endpoint tdaq::ipc::Partition::lookup(const std::string& name) const
{
    std::string s("resolve " + name);

    zmq::message_t request(s.size());
    memcpy(request.data(), s.c_str(), s.size());

    zmq::message_t reply = client().makeRequest(request);

    const char * data = reply.data<const char>();
    std::string r(data, reply.size());
    if (reply.size() && data[0] != 'n') {
        return Endpoint::fromString(r);
    }

    throw zmq::error_t();
}

