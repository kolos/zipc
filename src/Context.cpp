/*
 * Context.cpp
 *
 *  Created on: Apr 11, 2016
 *      Author: kolos
 */

#include <thread>

#include <ipc/Context.hpp>

::zmq::context_t& tdaq::ipc::Context::globalInstance()
{
    static ::zmq::context_t g_context;

    return g_context;
}
