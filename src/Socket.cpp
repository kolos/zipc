/*
 * Socket.cpp
 *
 *  Created on: Apr 11, 2016
 *      Author: kolos
 */

#include <iostream>
#include <ipc/Socket.hpp>

tdaq::ipc::Socket::Socket(::zmq::socket_type type, ::zmq::context_t& context)
    : ::zmq::socket_t(context, type),
      Context(context),
      Monitor(*this),
      m_type(type),
      m_failed(false)
{
}

void tdaq::ipc::Socket::on_event_connected(const zmq_event_t& /*event*/,
    const char* addr)
{
    std::clog << addr << " socket is connected" << std::endl;
    m_failed = false;
}

void tdaq::ipc::Socket::on_event_disconnected(const zmq_event_t& /*event*/,
    const char* addr)
{
    std::clog << addr << " socket is disconnected" << std::endl;
    m_failed = true;
}

void tdaq::ipc::Socket::on_event_connect_retried(const zmq_event_t& event,
        const char* addr)
{
    std::clog << addr << " retrying connection attempt" << std::endl;
    m_failed = true;
}

bool tdaq::ipc::Socket::canRead(int32_t timeout)
{
    return poll(ZMQ_POLLIN, timeout) && !m_failed;
}

bool tdaq::ipc::Socket::canWrite(int32_t timeout)
{
    return poll(ZMQ_POLLOUT, timeout) && !m_failed;
}

bool tdaq::ipc::Socket::poll(int event, int32_t timeout) {
    zmq::pollitem_t items[2];
    items[0].socket = (void*) *this;
    items[0].events = event | ZMQ_POLLERR;
    items[1].socket = (void*) m_monitor_socket;
    items[1].events = ZMQ_POLLIN | ZMQ_POLLERR;

    int r = zmq_poll(items, 2, timeout);

    if (r < 0) {
        m_failed = true;
        return false;           // may be throw here?
    }

    if (!r) {
        throw zmq::error_t(); // this is TIMEOUT
    }

    if (items[1].revents) {
        update();
    }

    if (items[0].revents & ZMQ_POLLERR) {
        std::clog << "recv: socket is in the error state" << std::endl;
        m_failed = true;
        return false;
    }

    if (items[0].revents & event) {
        return true;
    }

    return false;
}
