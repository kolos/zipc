/*
 * PersistentClient.cpp
 *
 *  Created on: Apr 20, 2016
 *      Author: kolos
 */

#include <iostream>

#include <ipc/PersistentClient.hpp>

tdaq::ipc::PersistentClient::PersistentClient(::zmq::socket_type type, Endpoint const& endpoint, ::zmq::context_t& context) :
    Client(type, endpoint, context)
{
    ;
}

tdaq::ipc::PersistentClient::~PersistentClient()
{
}

bool tdaq::ipc::PersistentClient::recover()
{
    try {
        ::zmq::context_t& c = m_socket->getContext();
        ::zmq::socket_type t = m_socket->getType();
        delete m_socket;
        m_socket = new Socket(t, c);
        Client::connect(m_endpoint);
        std::clog << "recovery succeeded with the same endpoint: "
                << m_endpoint.getAddress() << std::endl;
        return true;
    }
    catch (zmq::error_t& e) {
        std::cerr << "recovery failed: " << e.what() << std::endl;
        return false;
    }
    std::clog << "recovery failed" << std::endl;
    return false;
}
