package atlas.daq.ipc;

import java.util.Arrays;

public class TestServer implements IRemoteTest {
  @Override
  public void oneway_test() {
    System.out.println("oneway_test method is called");
  }

  @Override
  public void test() {
    System.out.println("test method is called");
  }

  @Override
  public void test1(int i) {
    System.out.println("test1 method is called with parameter: " + i);
  }

  @Override
  public void test2(int i, long l) {
    System.out.println("test2 method is called with parameters: " + i + " " + l);
  }

  @Override
  public void test3(int i, long l, String s) {
    System.out.println("test3 method is called with parameters: " + i + " " + l + " " + s);
  }

  @Override
  public Param testP(Param p) {
    System.out.println("testP method is called with parameter: " + p);
    return p;
  }

  @Override
  public int rtest1(int i) {
    System.out.println("rtest1 method is called with parameter: " + i);
    return 0;
  }

  @Override
  public long rtest2(int i, long l) {
    System.out.println("rtest2 method is called with parameters: " + i + " " + l);
    return 0;
  }

  @Override
  public String rtest3(int i, long l, String s) {
    System.out.println("rtest3 method is called with parameters: " + i + " " + l + " " + s);
    return null;
  }

  @Override
  public int[] artest1(int[] i) {
    System.out.println("artest1 method is called with parameter: " + Arrays.toString(i));
    return i;
  }

  @Override
  public long[] artest2(int[] i, long[] l) {
    System.out.println("artest2 method is called with parameters: " + Arrays.toString(i) + " " + Arrays.toString(l));
    return l;
  }

  @Override
  public String[] artest3(int[] i, long[] l, String[] s) {
    System.out.println("artest3 method is called with parameters: " + Arrays.toString(i) + " " + Arrays.toString(l) + " " + Arrays.toString(s));
    return s;
  }
  
  public static void main(String[] args) throws InterruptedException {
    Partition partition = new Partition();

    Server server = new Server.Builder(SocketType.rep, new TestServer()).workerThreads(3).build();
    partition.publish(server, "test");
    
    Thread.sleep(60000);
  }
  
}
