package atlas.daq.ipc;

import java.util.Arrays;

public class TestClient {
  static void test(Client client) throws InterruptedException {
    // Do N requests, waiting each time for a response
    for (int request_nbr = 0; request_nbr != 100; ++request_nbr) {
      String request = new String("x");

      System.out.println("sending request " + request_nbr + " ... ");
      client.send(request);

      System.out.println(" waiting for replay ... ");
      // Get the reply.
      String reply = client.recv();
      System.out.println("got it: " + reply);

      Thread.sleep(3000);
    }
  }

  static void test1(Partition p) throws InterruptedException {
    PublicClient client = new PublicClient(SocketType.req, p, "test");
    System.out.println("Connected to " + client.getEndpoint().getAddress());
    while (true) {
      try {
        test(client);
      } catch (Exception e) {
        System.out.println("Error: " + e.getMessage());
        Thread.sleep(1000);
      }
    }
  }

  public static void main(String[] args) throws InterruptedException {
    Partition partition = new Partition();

    IRemoteTest rt = partition.lookup(IRemoteTest.class, "test", SocketType.req);

    rt.test();
    rt.test1(100);
    rt.test2(300, 700000000);
    rt.test3(300, 700000000, "ABCD");

    int i = rt.rtest1(100);
    System.out.println("rtest1 returns " + i);
    long l = rt.rtest2(300, 700000000);
    System.out.println("rtest2 returns " + l);
    String s = rt.rtest3(300, 700000000, "ABCD");
    System.out.println("rtest3 returns " + s);

    int[] ar1 = { 0, 1, 2, 3 };
    long[] ar2 = { 0, 1, 2, 3 };
    String[] ar3 = { "A", "B", "C", "D" };

    int[] ii = rt.artest1(ar1);
    System.out.println("artest1 returns " + Arrays.toString(ii));
    long[] ll = rt.artest2(ar1, ar2);
    System.out.println("artest2 returns " + Arrays.toString(ll));
    String[] ss = rt.artest3(ar1, ar2, ar3);
    System.out.println("artest3 returns " + Arrays.toString(ss));

    IRemoteTest.Param p = rt.testP(new IRemoteTest.Param());
    System.out.println("testP returns " + p);
  }
}
