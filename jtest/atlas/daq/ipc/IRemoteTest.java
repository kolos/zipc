package atlas.daq.ipc;

public interface IRemoteTest {
  class Param {
    public int field1 = 0;
    public long field2 = 100000000;

    @Override
    public String toString() {
      return "Param [field1=" + field1 + ", field2=" + field2 + "]";
    }
  }

  void oneway_test();

  void test();
  
  void test1(int i);

  void test2(int i, long l);

  void test3(int i, long l, String s);

  Param testP(Param p);

  int rtest1(int i);

  long rtest2(int i, long l);

  String rtest3(int i, long l, String s);

  int[] artest1(int[] i);

  long[] artest2(int[] i, long[] l);

  String[] artest3(int[] i, long[] l, String[] s);
}
